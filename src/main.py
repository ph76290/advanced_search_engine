# Imports
from documents import *
from analyze import *
from indexer import *
from searcher import *

# The different parts to implement for the tutorials can be found in the related files: documents.py, analyze.py, indexer.py and searcher.py.

# Fetch documents
documents = fetch("../20news-bydate/20news-bydate-train", True)

# Documents tokenization
processors = [Normalizer()]
tokenized_documents = []
for document in documents:
    tokenized_documents.append(analyze(document, processors))

# Index creation + save the index in json
postings = index(tokenized_documents)
index = build(postings)
save(index, ".")

# You can use the three different searches
print(search('from:'))
print(searchAllOf(['from:', 're:', 'fuller)', 'interesting']))
print(searchOneOf(['from:', 're:']))
