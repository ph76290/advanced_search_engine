import json
import os


# The Posting class contains only 2 attributes

class Posting:
    
    def __init__(self, word, urls):
        self.word = word
        self.urls = urls

# The method index builds the list of Postings


def index(documents):
    dico = []
    tmp = dict()
    for document in documents:
        for word in document.words:
            if not word in tmp:
                tmp[word] = len(dico)
                dico.append(Posting(word, [document.url]))
            else:
                if not document.url in dico[tmp[word]].urls:
                    dico[tmp[word]].urls.append(document.url)
    return dico


# The class Index contains the 2 dictionaries
# I changed the prototype of one of them
# urlToDid became didToUrl because I did not understand how I could use the one in the guidelines
# It allows me to get the url from an id so then I am able to link wordsToDids to didToUrl to match each word to each url


class Index:

    def __init__(self, didToUrl, wordsToDids):
        self.didToUrl = didToUrl
        self.wordsToDids = wordsToDids


def build(postings):
    index = Index(dict(), dict())
    id_ = 0
    for posting in postings:
        tmp = set()
        for url in posting.urls:
            if not url in index.didToUrl:
                index.didToUrl[id_] = url
                id_ += 1
                tmp.add(len(index.didToUrl.keys()) - 1)
            else:
                tmp.add(index.didToUrl.index(url))
        if not posting.word in index.wordsToDids:
            index.wordsToDids[posting.word] = tmp

    return index
    

# set default is used for the serializing process


def set_default(data):
    if isinstance(data, dict) or isinstance(data, set):
        return list(data)
    else:
        return data

# This method serializes the dictionaries and creates the json


def save(index, path):
    fd = open(os.path.join(path, "words2Dids.json"), "w+")
    data = json.dumps(index.wordsToDids, default=set_default)
    fd.write(data)
    fd.close()

    fd = open(os.path.join(path, "did2Url.json"), "w+")
    data = json.dumps(index.didToUrl)
    fd.write(data)
    fd.close()

    print("Dumped")
