import os
import io

# I followed the exact prototypes for the implementations on the class Document and the method fetch
class Document:

    def __init__(self, text, url):
        self.text = text
        self.url = url


def fetch(path, recursive):
    documents = []
    for path, dirs, files in os.walk(path):
        for filename in files:
            try:
                with io.open(os.path.join(path, filename)) as f:
                    text = f.read()
                    documents.append(Document(text, os.path.join(path, filename)))
            except:
                continue
        if not(recursive):
            break
    return documents
