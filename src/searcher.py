import json
import os
from indexer import *


# The load method deserializes the objects and recreate the Index object from them


def load(path):
    didToUrl = dict()
    wordsToDids = dict()

    #with open(os.path.join(path, "did2Url.json")) as f:
    with open(os.path.join(path, "did2Url.json"), "r") as f:
        text = f.read()
        didToUrl = json.loads(text)

    with open(os.path.join(path, "words2Dids.json"), "r") as f:
        text = f.read()
        wordsToDids = json.loads(text)

    index = Index(didToUrl, wordsToDids)
    return index


# And then the 3 search functions are implemented, searchAllOf and searchOneOf were easily implemented thanks to the intersection and union methods on the sets


def search(word):
    index = load(".")
    ids = index.wordsToDids[word]
    urls = set(map(lambda x: index.didToUrl[str(x)], ids))
    return urls


def searchAllOf(words):
    index = load(".")
    final_set = set()
    for i in range(len(words)):
        ids = index.wordsToDids[words[i]]
        urls = set(map(lambda x: index.didToUrl[str(x)], ids))
        if i == 0:
            final_set = urls
        final_set = final_set.intersection(urls)
    return final_set



def searchOneOf(words):
    index = load(".")
    final_set = set()
    for i in range(len(words)):
        ids = index.wordsToDids[words[i]]
        urls = set(map(lambda x: index.didToUrl[str(x)], ids))
        if i == 0:
            final_set = urls
        final_set = final_set.union(urls)
    return final_set
