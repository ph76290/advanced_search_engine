from unidecode import unidecode

# TokenizedDocument only contains 2 attributes, TextProcessor is an abstract class and Normalizer extends the previous one

class TokenizedDocument:

    def __init__(self, words, url):
        self.words = words
        self.url = url


class TextProcessor():
    
    def process(self, word):
        pass


class Normalizer(TextProcessor):
    
    def process(self, word):
        return unidecode(word).lower()


# The method analyze applies the preprocessing on the document


def analyze(document, processors):
    words = document.text.split(" ")
    for processor in processors:
        words = list(map(lambda x: processor.process(x), words))
    return TokenizedDocument(words, document.url)
